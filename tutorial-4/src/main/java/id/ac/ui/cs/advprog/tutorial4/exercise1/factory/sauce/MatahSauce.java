package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class MatahSauce implements Sauce {
    public String toString() {
        return "Matah Sauce";
    }
}
