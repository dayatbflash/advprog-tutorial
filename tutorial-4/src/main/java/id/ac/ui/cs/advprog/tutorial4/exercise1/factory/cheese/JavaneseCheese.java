package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class JavaneseCheese implements Cheese {

    public String toString() {
        return "Javanese Cheese";
    }
}
