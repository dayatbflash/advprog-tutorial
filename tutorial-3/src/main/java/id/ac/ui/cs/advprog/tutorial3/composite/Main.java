package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.Company;
import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;

public class Main {
    public static void main(String[] args) {
        Company company = new Company();

        company.addEmployee(new Ceo("Dayat", 2000000000));
        company.addEmployee(new Cto("Dipo", 1000000000));
        company.addEmployee(new BackendProgrammer("Mawan", 300000));
        company.addEmployee(new FrontendProgrammer("Vano", 400000));

        System.out.println("LIST OF EMPLOYEES:");
        for (Employees employees : company.getAllEmployees()) {
            System.out.println(employees.getName() + " [" + employees.getRole() + "]");
        }
        System.out.println(String.format("TOTAL SALARIES: %f", company.getNetSalaries()));
    }
}