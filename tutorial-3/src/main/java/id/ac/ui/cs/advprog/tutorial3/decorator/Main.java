package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.BreadProducer;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.FillingDecorator;

public class Main {
    public static void main(String[] args) {
        Food food = null;
        food = BreadProducer.NO_CRUST_SANDWICH.createBreadToBeFilled();
        food = FillingDecorator.BEEF_MEAT.addFillingToBread(food);
        food = FillingDecorator.CHEESE.addFillingToBread(food);
        food = FillingDecorator.CHILI_SAUCE.addFillingToBread(food);

        // EXTRA SUPER DUPER CHIZZZZZZZ
        for (int i = 0; i < 20; i++) {
            food = FillingDecorator.CHEESE.addFillingToBread(food);
        }

        System.out.println(food.getDescription());
        System.out.println("TOTAL COST: " + food.cost());
    }
}